package net.codelab.data;

import net.codelab.Mundiales;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("codelabmafe")
public class MundialMongo {

    @Id
    public String id;
    public String nombre;
    public String pais;
    public int modelo;
    public String campeon;
    public int goles;

    public MundialMongo() {

    }

    public MundialMongo(String id, String nombre, String pais, int modelo, String campeon, int goles) {
        this.id = id;
        this.nombre = nombre;
        this.pais = pais;
        this.modelo = modelo;
        this.campeon = campeon;
        this.goles = goles;
    }

    @Override
    public String toString() {
        return String.format("Mundial [id=%s, nombre=%s, pais=%s, modelo=%s, campeon=%s, goles=%s", id, nombre, pais, modelo, campeon,goles);
    }
}
