package net.codelab.data;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;


import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MundialRepository extends MongoRepository<MundialMongo, String> {
    //Búsqueda por Nombre
    @Query("{'nombre':?0}")
    public List<MundialMongo> findByNombre(String nombre);

    //Búsqueda por goles del mundial
    @Query("{'goles':{$gt: ?0, $lt: ?1}}")
    public List<MundialMongo> findByGoles(int golesmin, int golesmax);

    @Query("{'nombre': ?0}")
    public Optional<MundialMongo> findByName(String nombre);

    @Query("{'nombre': ?0}")
    public List<MundialMongo> deleteByName(String nombre);
}
