package net.codelab;

import net.codelab.data.MundialMongo;
import net.codelab.data.MundialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController

public class MundialesV2Controller {

    @Autowired
    private MundialRepository repository;

    //GET: Lista de mundiales
    @GetMapping(value= "/v2/mundiales", produces = "application/json")
    public ResponseEntity<List<MundialMongo>> obtenerListado() {
        List<MundialMongo> lista = repository.findAll();
        return new ResponseEntity<List<MundialMongo>>(lista, HttpStatus.OK);
    }

    //GET: Buscar Mundial por nombre
    @GetMapping(value= "/v2/mundiales/{nombre}", produces = "application/json")
    public ResponseEntity<List<MundialMongo>> obtenerMundialPorNombre(@PathVariable String nombre) {
        List<MundialMongo> resultado = repository.findByNombre(nombre);
        return new ResponseEntity<List<MundialMongo>>(resultado, HttpStatus.OK);
    }

    //GET: Buscar Mundial por rango de goles
    @GetMapping(value= "/v2/mundiales/{golesmin}/{golesmax}", produces = "application/json")
    public ResponseEntity<List<MundialMongo>> obtenerMundialPorGoles(@PathVariable int golesmin, @PathVariable int golesmax) {
        List<MundialMongo> resultado = repository.findByGoles(golesmin, golesmax);
        return new ResponseEntity<List<MundialMongo>>(resultado, HttpStatus.OK);
    }

    //POST: Añadir un mundial
    @PostMapping(value = "/v2/mundiales/nuevo", produces = "application/json")
    public ResponseEntity<String> nuevoMundial (@RequestBody MundialMongo mundialMongo) {
        MundialMongo resultado = repository.insert(mundialMongo);
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }

    //PUT: Actualizar mundial
    @PutMapping(value = "/v2/mundiales/{nombre}", produces = "application/json")
    public ResponseEntity<String> actualizarMundial (@PathVariable String nombre, @RequestBody MundialMongo mundialMongo) {
        Optional<MundialMongo> resultado = repository.findByName(nombre);
        System.out.println("nombre a buscar: " + resultado);
        if (resultado.isPresent()){

            MundialMongo mundialAModificar = resultado.get();
            mundialAModificar.nombre = mundialMongo.nombre;
            mundialAModificar.pais = mundialMongo.pais;
            mundialAModificar.modelo = mundialMongo.modelo;
            mundialAModificar.campeon = mundialMongo.campeon;
            mundialAModificar.goles = mundialMongo.goles;
            MundialMongo saved = repository.save(resultado.get());
            return new ResponseEntity<String>(resultado.toString(),HttpStatus.OK);
        }
        return new ResponseEntity<String>("Mundial no encontrado", HttpStatus.NOT_FOUND);
    }

    //DELETE: Borrar todos los mundiales
    @DeleteMapping(value = "/v2/mundiales/delete", produces = "application/json")
    public ResponseEntity<String> deleteAllMundial(){
        repository.deleteAll();
        return new ResponseEntity<String>("Todos los Mundiales se han borrado", HttpStatus.OK);
    }

    //DELETE: Borrar un Mundial por nombre
    @DeleteMapping(value = "/v2/mundiales/delete/{id}", produces = "application/json")
    public ResponseEntity<String> deleteMundial(@PathVariable String id) {
        Optional<MundialMongo> resultado = repository.findById(id);
        if (resultado.isPresent()) {
            repository.deleteById(id);
            return new ResponseEntity<String>("Mundial " + id + " borrado", HttpStatus.OK);
        }
        return new ResponseEntity<String>("Mundial " + id + " No borrado", HttpStatus.NO_CONTENT);
    }

}
