package net.codelab;

import org.springframework.http.HttpRange;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController

public class MundialesController {

    private ArrayList<Mundiales> listaMundiales = null;

    public MundialesController() {
        listaMundiales = new ArrayList<>();
        listaMundiales.add(new Mundiales(1, "Uruguay1930", "Uruguay", 1930, "Uruguay", 0));
        listaMundiales.add(new Mundiales(2, "Italia1934", "Italia", 1934, "Italia", 0));
    }

    //GET: Lista de mundiales
    @GetMapping(value = "/mundiales", produces = "application/json")
    public ResponseEntity<List<Mundiales>> obtenerListado() {
        return new ResponseEntity<>(listaMundiales, HttpStatus.OK);
    }

    //GET: Lista por año
    @GetMapping("/mundiales/{id}")
    public ResponseEntity<List<Mundiales>> obtenerListadomodel(@PathVariable int id) {
        Mundiales resultado = null;
        ResponseEntity<List<Mundiales>> respuesta = null;
        String mensaje = "";
        try {
            resultado = listaMundiales.get(id);
            respuesta = new ResponseEntity(resultado, HttpStatus.OK);
        } catch (Exception ex)
        {
            mensaje = "No se ha encontrado el mundial por año";
            respuesta = new ResponseEntity(mensaje, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

    //ADD: Agregar un nuevo mundial
    @PostMapping(value = "/mundiales/nuevo", produces="application/json")
    public ResponseEntity<String> addMundial(@RequestBody Mundiales mundialNuevo) {
    //    listaProductos.add(new Mundiales(3,"Francia1934","Francia",1938, "Italia", 0));
        listaMundiales.add(mundialNuevo);
        return new ResponseEntity<String>("Mundial creado correctamente", HttpStatus.CREATED);
    }

    //PUT: Actualizar goles mundial
    @PutMapping(value = "/mundiales", produces = "application/json")
    public ResponseEntity<String> updateMundial(@RequestBody Mundiales cambios)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            Mundiales mundialAModificar = listaMundiales.get(cambios.getId());
            mundialAModificar.setNombre(cambios.getNombre());
            mundialAModificar.setGoles(cambios.getGoles());
            listaMundiales.set(cambios.getId(), mundialAModificar);
            resultado = new ResponseEntity<>("Mundial Actualizado",HttpStatus.OK);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity("No se ha encontrado el producto a actualizar", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }
    //DELETE: Eliminar mundial
    @DeleteMapping(value = "/mundiales/{id}", produces = "application/json")
    public ResponseEntity<String> deleteMundial(@PathVariable int id){
        ResponseEntity<String> resultado = null;
        try {
            Mundiales mudialAEliminar = listaMundiales.get(id-1);
            listaMundiales.remove(id-1);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex){
            resultado = new ResponseEntity<>("No se ha encontrado el mundial", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }
}
