package net.codelab;

public class Mundiales {

    private int id;
    private String nombre;
    private String pais;
    private int modelo;
    private String campeon;
    private int goles;

    public Mundiales(int id, String nombre, String pais, int modelo, String campeon, int goles) {
        this.setId(id);
        this.setNombre(nombre);
        this.setPais(pais);
        this.setModelo(modelo);
        this.setCampeon(campeon);
        this.setGoles(goles);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public int getModelo() {
        return modelo;
    }

    public void setModelo(int modelo) {
        this.modelo = modelo;
    }

    public String getCampeon() {
        return campeon;
    }

    public void setCampeon(String campeon) {
        this.campeon = campeon;
    }

    public int getGoles() {
        return goles;
    }

    public void setGoles(int goles) {
        this.goles = goles;
    }
}



